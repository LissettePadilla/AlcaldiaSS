<?php

class modelSolicitudCaso{

  private $idSolicitudCaso;
  private $descripcionCaso;
  private $fechaCaso;
  private $idDetaEmpleadoSolicitante;
  private $idEstadoCaso;

  public function getIdSolicitudCaso(){
    return $this->idSolicitudCaso;
  }

  public function setIdSolicitudCaso($idSolicitudCaso){
    $this->idSolicitudCaso = $idSolicitudCaso;
  }

  public function getDescripcionCaso(){
    return $this->descripcionCaso;
  }

  public function setDescripcionCaso($descripcionCaso){
    $this->descripcionCaso = $descripcionCaso;
  }

  public function getFechaCaso(){
    return $this->fechaCaso;
  }

  public function setFechaCaso($fechaCaso){
    $this->fechaCaso = $fechaCaso;
  }

  public function getIdDetaEmpleadoSolicitante(){
    return $this->idDetaEmpleadoSolicitante;
  }

  public function setIdDetaEmpleadoSolicitante($idDetaEmpleadoSolicitante){
    $this->idDetaEmpleadoSolicitante = $idDetaEmpleadoSolicitante;
  }

  public function getIdEstadoCaso(){
    return $this->idEstadoCaso;
  }

  public function setIdEstadoCaso($idEstadoCaso){
    $this->idEstadoCaso = $idEstadoCaso;
  }


  function create(){
    $description_caso=$this->getDescripcionCaso();
    $fecha_caso=$this->getFechaCaso();
    $id_deta_empleado_solicitante=$this->getidDetaEmpleadoSolicitante();
    $id_estado_caso=$this->getIdEstadoCaso();

     $objConnection = new Connection();
     $sql="SELECT * FROM HELPDESK_INSERT_SOLICITUDCASO($description_caso, $fecha_caso, $id_deta_empleado_solicitante, $id_estado_caso)";
     $result=$objConnection->Consultas($sql);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }

     return $result;
  }

  function update($id){

    $id_estado_caso=$this->getIdEstadoCaso();

    $objConnection = new Connection();
    $sql="SELECT * FROM HELPDESK_UPDATE_SOLICITUDCASO";
    $result=$objConnection->Consultas($sql);

    if (!$result) {
      echo "An error occurred.\n";
      exit;
    }

     return $result;
  }

}




 ?>
