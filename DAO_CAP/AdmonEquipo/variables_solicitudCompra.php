<?php

include("../../DAO_CAP/Conexion/admon_conexion.php");


class EncaSolicitudCompra{
    private $descSoli ="";
    private $idCaso = "";
    private $idEmpSolicitante="";
    // la fecha y estado de solicitud se haran internamente aca
    private $idSolicitudCompra="";
    private $idEmpAprov="";
    
    public function getDescSoli() {
        return $this->descSoli;
    }

    public function getIdCaso() {
        return $this->idCaso;
    }

    public function getIdEmpSolicitante() {
        return $this->idEmpSolicitante;
    }


    public function setDescSoli($descSoli) {
        $this->descSoli = $descSoli;
    }

    public function setIdCaso($idCaso) {
        $this->idCaso = $idCaso;
    }

    public function setIdEmpSolicitante($idEmpSolicitante) {
        $this->idEmpSolicitante = $idEmpSolicitante;
    }
    
    public function getIdSolicitudCompra() {
        return $this->idSolicitudCompra;
    }

    public function setIdSolicitudCompra($idSolicitudCompra) {
        $this->idSolicitudCompra = $idSolicitudCompra;
    }
    
    public function getIdEmpAprov() {
        return $this->idEmpAprov;
    }

    public function setIdEmpAprov($idEmpAprov) {
        $this->idEmpAprov = $idEmpAprov;
    }

    
    //-------------- metodos --------------------
    function IngresarEncaSolicitudCompra(){
        $fcnDesc = $this->getDescSoli();
        $fcnIdCaso = $this->getIdCaso();
        $fcnIdEmp = $this->getIdEmpSolicitante();
        $idInsertado="";
        
        
        $queryEstadoSoli = "SELECT ID_ESTADO_COMPRA FROM ESTADO_SOLI_COMPRA WHERE DESCRIPCION LIKE '%PENDI%'"; //query lineal
        $rsEstado = pg_query($queryEstadoSoli);
        $idEstadoSolicitud = pg_fetch_result($rsEstado, 0); // AQUI ESTA EL ESTADO PENDIENDE VALIDAR
        
        $queryInsert = "SELECT Admon_Insertar_Enca_Soli_Compra('$fcnDesc',$idEstadoSolicitud,$fcnIdEmp,$fcnIdCaso)"; //funcion
        
        
        
        if(pg_query($queryInsert)){
            $queryGetIdInsertado = "SELECT MAX(ID_SOLICICTUD_COMPRA) FROM SOLICITUD_COMPRA WHERE ID_DETA_EMPLEADO = $fcnIdEmp"; //query lineal
            $rsIdInsertado = pg_query($queryGetIdInsertado);
            $idInsertado = pg_fetch_result($rsIdInsertado, 0);
        }  else {
            $idInsertado=NULL;
        }
        
        return $idInsertado;

    }
    
     function ValidarIdCaso(){ // esta funcion sirve para verificar si el id de BITACORA DE caso que escriben es real
        $fcnIdCaso = $this->getIdCaso();
        
        $query = "SELECT ID_BITACORA_CASO FROM BITACORA_CASO A
                  INNER JOIN SOLICITUD_CASO B ON (A.ID_SOLICITUD_CASO = B.ID_SOLICITUD_CASO)
                  WHERE B.ID_ESTADO_CASO = 2
                  AND A.ID_BITACORA_CASO = $fcnIdCaso"; //query lineal
        $rsIdBitacora = pg_query($query);
        $IdBitacora = pg_fetch_result($rsIdBitacora, 0);
        
        return $IdBitacora;
        
    }
    
    function CancelarSoliCompra(){ //funcion hecha para que el usuario cancele su solicitud de compra
        $fcnIdSoliCompra = $this->getIdSolicitudCompra();
        
        $queryEstado = "SELECT ID_ESTADO_COMPRA FROM ESTADO_SOLI_COMPRA WHERE DESCRIPCION LIKE '%CANC%%USU%'"; //query lineal
        $rsEstado = pg_query($queryEstado);
        $estadoCanc = pg_fetch_result($rsEstado, 0);
        
        $queryCanc = "SELECT Admon_CancelarUsuario_Soli_Compra($fcnIdSoliCompra,$estadoCanc)"; //funcion
        pg_query($queryCanc);
    }
    
    function AprovarSoliCompra(){ //funcion hecha para usuario con permiso de aprobar la solicitud
        $fcnIdSoliCompra = $this->getIdSolicitudCompra();
        $fcnIdEmpAprovo = $this->getIdEmpAprov();
        
        $queryEstadoAprov = "SELECT ID_ESTADO_COMPRA FROM ESTADO_SOLI_COMPRA WHERE DESCRIPCION LIKE '%APROV%'"; //query lineal
        $rsEstadoAprov = pg_query($queryEstadoAprov);
        $idEstadoAprov = pg_fetch_result($rsEstadoAprov, 0);
        
        $queryAprovar = "SELECT Admon_Aprovar_Denegar_Soli_Compra($fcnIdSoliCompra,$idEstadoAprov,$fcnIdEmpAprovo)";
        pg_query($queryAprovar);
    }
    
    function DenegarSoliCompra(){ // funcion hecha para usuario con permiso de denegar solicitudes
        $fcnIdSoliCompra = $this->getIdSolicitudCompra();
        $fcnIdEmpDenego = $this->getIdEmpAprov();
        
        $queryEstadoAprov = "SELECT ID_ESTADO_COMPRA FROM ESTADO_SOLI_COMPRA WHERE DESCRIPCION LIKE '%DENEG%'"; //query lineal
        $rsEstadoAprov = pg_query($queryEstadoAprov);
        $idEstadoAprov = pg_fetch_result($rsEstadoAprov, 0);
        
        $queryAprovar = "SELECT Admon_Aprovar_Denegar_Soli_Compra($fcnIdSoliCompra,$idEstadoAprov,$fcnIdEmpDenego)";
        pg_query($queryAprovar);
    }
    
    function ValidarAprovDenegSoliCompra(){
        $fcnIdSoliCompraValid = $this->getIdSolicitudCompra();
        
        $query = "SELECT * FROM Admon_Validar_Soli_Compra_AprovDeneg($fcnIdSoliCompraValid)";
        $rsValidacion = pg_query($query);
        $DatoValidacion = pg_fetch_result($rsValidacion, 0);
        
        return $DatoValidacion;
    }
    
    
} //FIN CLASE EncaSolicitudCompra

class DetaSolicitudCompra{
    private $idSolicitud="";
    private $descDetalle="";
    private $descArticulo="";
    private $cantidad="";
    private $idDetaSoli="";
    
    public function getIdSolicitud() {
        return $this->idSolicitud;
    }

    public function getDescDetalle() {
        return $this->descDetalle;
    }

    public function getDescArticulo() {
        return $this->descArticulo;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public function setIdSolicitud($idSolicitud) {
        $this->idSolicitud = $idSolicitud;
    }

    public function setDescDetalle($descDetalle) {
        $this->descDetalle = $descDetalle;
    }

    public function setDescArticulo($descArticulo) {
        $this->descArticulo = $descArticulo;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
    
    public function getIdDetaSoli() {
        return $this->idDetaSoli;
    }

    public function setIdDetaSoli($idDetaSoli) {
        $this->idDetaSoli = $idDetaSoli;
    }

    
    //-------------
    function IngresarDetaSolicitudCompra(){
        $fcnIdSolicitud = $this->getIdSolicitud();
        $fcnDescDetalle = $this->getDescDetalle();
        $fcnDescArticulo = $this->getDescArticulo();
        $fcnCantidad = $this->getCantidad();
        
        $queryInsert = "SELECT Admon_Insertar_Deta_Soli_Compra('$fcnDescDetalle','$fcnDescArticulo','$fcnCantidad',$fcnIdSolicitud)";
        
        pg_query($queryInsert);
    }
    
    function QuitarLineaDetaSoliCompra(){
        $fcnIdDetaSoli = $this->getIdDetaSoli();
        
        $query = "DELETE FROM DETA_SOLICITUD_COMPRA WHERE ID_DETA_COMPRA = $fcnIdDetaSoli";
        pg_query($query);
    }
} //FIN CLASE DetaSolicitudCompra

?>