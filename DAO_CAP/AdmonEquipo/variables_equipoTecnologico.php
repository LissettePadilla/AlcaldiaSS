3<?php

include("../../DAO_CAP/Conexion/admon_conexion.php");

class EquipoTecnologico{
    private $descEquipo="";
    private $codEquipo="";
    private $idTipoEquipo="";
    private $idEstadoEquipo="";
    private $idTipoAdquisicion="";
    private $idModelo="";
    private $idEmpCreaModif="";
    private $idEquipo = "";
    
    public function getDescEquipo() {
        return $this->descEquipo;
    }

    public function getCodEquipo() {
        return $this->codEquipo;
    }

    public function getIdTipoEquipo() {
        return $this->idTipoEquipo;
    }

    public function getIdEstadoEquipo() {
        return $this->idEstadoEquipo;
    }

    public function getIdTipoAdquisicion() {
        return $this->idTipoAdquisicion;
    }

    public function getIdModelo() {
        return $this->idModelo;
    }

    public function getIdEmpCreaModif() {
        return $this->idEmpCreaModif;
    }
    
    public function getIdEquipo() {
        return $this->idEquipo;
    }

    public function setDescEquipo($descEquipo) {
        $this->descEquipo = $descEquipo;
    }

    public function setCodEquipo($codEquipo) {
        $this->codEquipo = $codEquipo;
    }

    public function setIdTipoEquipo($idTipoEquipo) {
        $this->idTipoEquipo = $idTipoEquipo;
    }

    public function setIdEstadoEquipo($idEstadoEquipo) {
        $this->idEstadoEquipo = $idEstadoEquipo;
    }

    public function setIdTipoAdquisicion($idTipoAdquisicion) {
        $this->idTipoAdquisicion = $idTipoAdquisicion;
    }

    public function setIdModelo($idModelo) {
        $this->idModelo = $idModelo;
    }

    public function setIdEmpCreaModif($idEmpCreaModif) {
        $this->idEmpCreaModif = $idEmpCreaModif;
    }

    public function setIdEquipo($idEquipo) {
        $this->idEquipo = $idEquipo;
    }

        
    //-----------------------------------------
    
    function ValidarCodEquipo(){
        $fcnCodEquipo = $this->getCodEquipo();
        $query = "SELECT CODIGO_EQUIPO FROM EQUIPO_TECNOLOGICO WHERE CODIGO_EQUIPO = '$fcnCodEquipo'";
        $rsCod = pg_query($query);
        $ExisteCod = pg_fetch_result($rsCod, 0);
        
        return $ExisteCod;
        
    }
   

    function IngresarEquipoTec(){
        $fcnDescEquipo= $this->getDescEquipo();
        $fcnCodEquipo= $this->getCodEquipo();
        $fcnIdTipoEquipo= $this->getIdTipoEquipo();
        $fcnIdModelo= $this->getIdModelo();
        $fcnIdEstadoEquipo= $this->getIdEstadoEquipo();
        $fcnIdTipoAdquisicion= $this->getIdTipoAdquisicion();
        $fcnIdEmpCreaModif= $this->getIdEmpCreaModif();
         
        // el primer parametro es el valor del case en la funcion plpgsql y el ultimo es el id del equipo
        $queryInsertar = 
                "select Admon_Insertar_EquipoTecnologico('$fcnDescEquipo','$fcnCodEquipo',$fcnIdTipoEquipo,$fcnIdModelo,$fcnIdEstadoEquipo,$fcnIdTipoAdquisicion,$fcnIdEmpCreaModif,0)";
        
        $rsIdEquipo = pg_query($queryInsertar);
        $idEquipoInsertado = pg_fetch_result($rsIdEquipo, 0);
        
        $queryTransaccionIngresoEquipo = "select Admon_Insertar_Transaccion_IngresarEquipo($idEquipoInsertado,$fcnIdEmpCreaModif)";
        $rsIdTransaccionIngreso = pg_query($queryTransaccionIngresoEquipo);
        $idTransaccionIngresoEq = pg_fetch_result($rsIdTransaccionIngreso, 0);
        
        $queryInsertarBitacora = "INSERT INTO BITACORA_EQUIPO (
                                    ID_DETA_EMPLEADO_ASIGNADO,
                                    ID_TRANSACCION_EQUIPO,
                                    UBICACION_ACTUAL)
                                    VALUES
                                    (NULL,$idTransaccionIngresoEq,TRUE)";
        pg_query($queryInsertarBitacora);
    }
    
    function SelectFullDataEquipo(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_DatosFull_Equipo($fcnIdEquipo)";
        $rsDatos = pg_query($query);
        
        return $rsDatos;
    }
    
    function SelectDetalleSoftware(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_SoftwareInstalado_Eq($fcnIdEquipo)";
        $rsSoft = pg_query($query);
        
        return $rsSoft;
    }
    
    function SelectBitacoraEquipoRelacionado(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_Bitacora_Relacion_Equipo($fcnIdEquipo)";
        $rsRela = pg_query($query);
        
        return $rsRela;
    }
    
    function SelectDetalleEquipoRelacionado(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_Equipo_Relacionado_2($fcnIdEquipo)";
        $rsRela2 = pg_query($query);
        
        return $rsRela2;
    }
    
    function SelectBitacoraUbicaciones(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_Bitacora_Ubicacion($fcnIdEquipo)";
        $rsBitacoraEquipo = pg_query($query);
        
        return $rsBitacoraEquipo;
    }
    
    
}

?>

