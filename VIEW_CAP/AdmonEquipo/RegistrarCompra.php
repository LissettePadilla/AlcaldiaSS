
<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>
    
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>REGISTRAR COMPRA</h2></center>
                          </header>
                          
                          <div class="panel-body"> <!-- div 3-->
                                <div class="form-group">
                                  <label><h3>Datos de Solicitud</h3></label>
                                  <header class="panel-heading">
                          </header>
                          <table class="table">
                              <thead>
                              <tr>
                                  <th>Id</th>
                                  <th>Fecha</th>
                                  <th>Descripción</th>
                                  <th>Estado</th>
                                  <th>Cod Solicitante</th>
                                  <th>id Caso</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <th>1</th>
                                  <th>yyyy-mm-dd</th>
                                  <th>texto...</th>
                                  <th>APROBADA</th>
                                  <th>ABD123456</th>
                                  <th> </th>
                              </tr>
                              </tbody>
                              </table>
                                  </div>
                              <div class="form-group">
                                  <label><h3>Detalle de Solicitud</h3></label>
                                  <header class="panel-heading">
                          </header>
                          <table class="table">
                              <thead>
                              <tr>
                                  <th>Id</th>
                                  <th>Descipción Detalle</th>
                                  <th>Descripción Item</th>
                                  <th>Cantidad</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <th>1</th>
                                  <th>Computadora pada administrador de sistemas</th>
                                  <th>HP probook</th>
                                  <th>1</th>
                              </tr>
                              <tr>
                                  <th>2</th>
                                  <th>Memoria RAM adicional para computadora HP</th>
                                  <th>memoria RAM 8gb</th>
                                  <th>1</th>
                              </tr>
                              </tbody>
                              </table>
                                  </div>
                          </div> <!-- div 3-->

                         
                      </section>
                  </div> <!-- div 2-->
              </div>  <!-- div 1-->
              <div class="row">
                <div class="col-log-12">
                  <section class="panel">
                    <header class="panel-heading"><center><h2>DATOS COMPRA</h2></center></header>
                  </section>
                </div>
              </div>
            <form method="get"> <!-- FORM -->
            <div class="row">
               <div class="col-lg-3">
                        <section class="panel">
                          <header class="panel-heading"><h5>Documento de Compra</h5></header>
                          <div class="panel-body">
                              <input type="text" size="20" maxlength="30" class="form-control" 
                                   placeholder="Ingrese documento de compra" />
                          </div>
                        </section>
                </div>
                <div class="col-lg-3">
                        <section class="panel">
                          <header class="panel-heading"><h5>Tipo de Documento</h5></header>
                          <div class="panel-body">
                            <select class="form-control m-bot15">
                            <option>Credito Fiscal</option>
                            <option>Consumidor Final</option>
                            <option>Factura comercial</option>
                            <option>Ticket</option>
                        </select>
                          </div>
                        </section>
                </div>
                <div class="col-lg-3">
                        <section class="panel">
                          <header class="panel-heading"><h5>Fecha de Compra</h5></header>
                          <div class="panel-body">
                              <input type="date" size="20" maxlength="30" class="form-control" 
                                   placeholder="Ingrese el documento de compra" />
                          </div>
                        </section>
                </div>
                <div class="col-lg-3">
                        <section class="panel">
                          <header class="panel-heading"><h5>Total de Compra</h5></header>
                          <div class="panel-body">
                              <input type="text" size="5" maxlength="9" class="form-control" 
                                   placeholder="Ingrese el monto de compra" />
                          </div>
                        </section>
                </div>
            </div>

             <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <div class="panel-body"> <!-- div 3-->

                                  <div class="adv-table editable-table ">
                                    <div class="clearfix">
                                        <div class="btn-group">
                                            <button id="editable-sample_new" class="btn green">
                                                Nueva Linea <i class="icon-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="space15"></div>
                                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                                        <thead>
                                        <tr>
                                            <th>Cod Equipo</th>
                                            <th>Cantidad</th>
                                            <th>Precio Unitario</th>
                                            <th>Editar linea</th>
                                            <th>Quitar linea</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                  </div>
                          </div> <!-- div 3-->
                      </section>
                  </div> <!-- div 2-->
                    <div>
                                  <center>
                                          <button type="submit" class="btn btn-success">Registrar Compra</button>
                                          <button type="reset" class="btn btn-danger">Cancelar Compra</button>
                                  </center>
                                  </div>
        </form>
              </div>  <!-- div 1-->
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
  <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>

    <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
    <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
    <script src="../js/editable-table-RegistrarCompra.js"></script>
    <script>
          jQuery(document).ready(function() {
              EditableTable.init();
          });
    </script>

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
        autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
