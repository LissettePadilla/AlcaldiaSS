
<!DOCTYPE html>
<html lang="en">
    <?php include '../import_css.php'; ?>

    <body>

        <section id="container" >
            <!--Comienza el Header-->
            <div class="header white-bg">
                <!--Inicio del Logo-->
                <div class="header">
                    <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                    <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                    <?php include '../Session.php' ?>
                </div>
                <!--Finaliza logo-->
            </div>
            <!--header end-->

            <!-- Main -->
            <?php include '../main.php'; ?>
            <!-- /End Main -->

            <!--Comienza contenido principal-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row"> <!-- div 1-->
                        <div class="col-lg-12"> <!-- div 2-->
                            <!--widget start-->
                            <section class="panel">
                                <center><h2>DETALLE DE EQUIPOS TECNOLÓGICOS</h2></center>
                                <header class="panel-heading tab-bg-dark-navy-blue">

                                    <ul class="nav nav-tabs nav-justified ">
                                        <li class="active">
                                            <a href="#DetalleEquipo" data-toggle="tab">
                                                Detalle de Equipo
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#DetalleEquipoCpu" data-toggle="tab">
                                                Detalle de Computadora 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#DetalleSoftware" data-toggle="tab">
                                                Detalle de Software Instalado
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#DetalleRelacionado" data-toggle="tab">
                                                Detalle de Equipo Relacionado
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#DetalleBitacoraUbicaciones" data-toggle="tab">
                                                Detalle de Ubicaciones del equipo
                                            </a>
                                        </li>
                                    </ul>
                                </header>
                                <div class="panel-body">
                                    <div class="tab-content tasi-tab">
                                        <div class="tab-pane active" id="DetalleEquipo">
                                            <form action="ModificarEquipoTec.php" method="POST"> <!-- FORM -->

                                                <div class="form-group">
                                                    <label><h4>Detalle de Equipo</h4></label>
                                                    <?php
                                                    include '../../DAO_CAP/AdmonEquipo/variables_equipoTecnologico.php';
                                                    $equipoSeleccionado = $_SESSION['IdEquipoParaVer'];
                                                    
                                                    //----- instancia a la clase
                                                    $objDatosEquiposFullEquipos = new EquipoTecnologico();
                                                    //----- Seteamos el id de equipo para trabajar
                                                    $objDatosEquiposFullEquipos->setIdEquipo($equipoSeleccionado);
                                                           
                                                    $rsDetaEquipo = $objDatosEquiposFullEquipos->SelectFullDataEquipo();
                                                    $rowDetaEquipo = pg_fetch_array($rsDetaEquipo);
                                                    ?>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Id Equipo: </label>
                                                            <input type="text" name="idEquipoModificar" hidden readonly>
                                                            <?php print $rowDetaEquipo['id_eq']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Codigo Equipo: </label>
                                                            <?php print $rowDetaEquipo['cod_eq']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Descripción Equipo: </label>
                                                            <?php print $rowDetaEquipo['desc_eq']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Fecha Creación: </label>
                                                            <?php print $rowDetaEquipo['fecha_crea']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Creado por : </label>
                                                            <?php print $rowDetaEquipo['emp_crea']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Tipo Equipo: </label>
                                                            <?php print $rowDetaEquipo['m_tipo_eq']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Detalle Tipo Equipo: </label>
                                                            <?php print $rowDetaEquipo['tipo_eq']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Estado Equipo: </label>
                                                            <?php print $rowDetaEquipo['estado_eq']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Tipo Adquisición: </label>
                                                            <?php print $rowDetaEquipo['tipo_adq']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Marca: </label>
                                                            <?php print $rowDetaEquipo['marca']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Modelo: </label>
                                                            <?php print $rowDetaEquipo['modelo']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Esta Relacionado con: </label>
                                                            <?php print $rowDetaEquipo['eq_rela']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Fecha Modificación: </label>
                                                            <?php print $rowDetaEquipo['fecha_modif']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <label class=" p-head">Modificado por: </label>
                                                            <?php print $rowDetaEquipo['emp_modif']; ?>
                                                        </div>
                                                    </article>
                                                    <article class="media">
                                                        <div class="media-body">
                                                            <button type="submit" name="ModificarEquipo" class="btn btn-info">Modificar Equipo</button>
                                                            <button type="submit" name="DesactivarEquipo" class="btn btn-danger">Desactivar Equipo</button>
                                                        </div>
                                                    </article>
                                                    
                                                </div>
                                            </form> <!-- FINALIZA FORM DE DETALLE EQUIPO -->
                                        </div>
                                        <div class="tab-pane" id="DetalleEquipoCpu">

                                        </div>
                                        <div class="tab-pane" id="DetalleSoftware">
                                            <div class="form-group">
                                                <label><h4>Detalle de Software Instalado</h4></label>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Descripción</th>
                                                            <th>Tipo</th>
                                                            <th>Activo</th>
                                                            <th>Fecha Activación</th>
                                                            <th>Fecha Desactivación</th>
                                                            <th>Id Empleado activó</th>
                                                            <th>Id Empleado Desactivó</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <?php
                                                            
                                                            $rsTabla = $objDatosEquiposFullEquipos->SelectDetalleSoftware();
                                                            while ($rowTabla = pg_fetch_assoc($rsTabla)) {
                                                                ?>
                                                                <th><?php print $rowTabla['id_eq_soft'] ?></th>
                                                                <th><?php print $rowTabla['desc_soft'] ?></th>
                                                                <th><?php print $rowTabla['tipo_soft'] ?></th>
                                                                <th><?php print $rowTabla['is_active'] ?></th>
                                                                <th><?php print $rowTabla['fecha_ins'] ?></th>
                                                                <th><?php print $rowTabla['fecha_desins'] ?></th>
                                                                <th><?php print $rowTabla['id_emp_ins'] ?></th>
                                                                <th><?php print $rowTabla['id_emp_desins'] ?></th>
                                                            </tr>
                                                        </tbody>
                                                    <?php } ?>
                                                </table>
                                            </div> 
                                        </div>
                                        <div class="tab-pane " id="DetalleRelacionado">
                                            <div class="panel-body">
                                                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                <h4 class="modal-title">Bitacora de equipos relacionados</h4>
                                                            </div>
                                                            <div class="modal-body">

                                                                <form role="form">
                                                                    <div class="form-group">
                                                                        <table class="table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Fecha</th>
                                                                                    <th>Codigo Equipo Relacionado</th>
                                                                                    <th>Realizado por</th>
                                                                                    <th>Operacion</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <?php
                                                                                    
                                                                                    $rsBitaRela = $objDatosEquiposFullEquipos->SelectBitacoraEquipoRelacionado();

                                                                                    while ($rowBita = pg_fetch_array($rsBitaRela)) {
                                                                                        ?>
                                                                                        <th><?php print $rowBita['fecha']; ?></th>
                                                                                        <th><?php print $rowBita['equipo_asig']; ?></th>
                                                                                        <th><?php print $rowBita['codigo_emp']; ?></th>
                                                                                        <th><?php print $rowBita['operation']; ?></th>
                                                                                    </tr>
                                                                                    <tr>
                                                                                    </tr>
                                                                                </tbody>
                                                                            <?php } ?>
                                                                        </table>
                                                                    </div>
                                                                 </form> 
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <label><h4>Detalle de Equipos Relacionados Actualmente:</h4></label>
                                                <header class="panel-heading"></header>
                                                <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-success">
                                                    VER BITACORAS DE EQUIPOS RELACIONADOS
                                                </a>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Id Equipo</th>
                                                            <th>Codigo Equipo</th>
                                                            <!--<th>Descripción Equipo</th>-->
                                                            <th>Tipo Equipo</th>
                                                            <th>Detalle Tipo Equipo</th>
                                                            <th>Estado</th>
                                                            <th>Equipo Relacionado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <?php
                                                            $rsEquipo2D = $objDatosEquiposFullEquipos->SelectDetalleEquipoRelacionado();

                                                            while ($rowEquipo2 = pg_fetch_assoc($rsEquipo2D)) {
                                                                ?>
                                                                <th><?php print $rowEquipo2['id_eq']; ?></th>
                                                                <th><?php print $rowEquipo2['cod_eq']; ?></th>
                                                               <!-- <th><?php print $rowEquipo2['desc_eq']; ?></th> -->
                                                                <th><?php print $rowEquipo2['m_tipo_eq']; ?></th>
                                                                <th><?php print $rowEquipo2['tipo_eq']; ?></th>
                                                                <th><?php print $rowEquipo2['estado_eq']; ?></th>
                                                                <th><?php print $rowEquipo2['id_eq_rela']; ?></th>
                                                            </tr>
                                                        </tbody>
                                                        <?php } ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="tab-pane " id="DetalleBitacoraUbicaciones">
                                            <div class="panel-body">
                                                
                                            <div class="form-group">
                                                <label><h4>Detalle de Ubicaciones de equipo</h4></label>
                                                <header class="panel-heading"></header>
                                                
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Fecha Ubicación</th>
                                                            <th>Transacción</th>
                                                            <th>Distrito Origen</th>
                                                            <th>Area del distrito Origen</th>
                                                            <th>Distrito Destino</th>
                                                            <th>Area del distrito Destino</th>
                                                            <th>Empleado Asignado</th>
                                                            <th>Ubicación actual</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <?php
                                                            $rsBitacoraUbicacion = $objDatosEquiposFullEquipos->SelectBitacoraUbicaciones();
                                                            $numRowsUbicacion = pg_num_rows($rsBitacoraUbicacion);
                                                            //while ($rowEquipoUbicacion = pg_fetch_assoc($rsBitacoraUbicacion)) {
                                                            for ($varControl = 0 ; $varControl< $numRowsUbicacion ; $varControl++) {
                                                                //$idTransModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'id_trans');
                                                                //$idEquipoModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'id_eq');
                                                                $fechaUbicacionModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'fecha_ubic');
                                                                $descTransModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'deta_trans');
                                                                $tipoTransModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'tipo_trans');
                                                                $empAutorizoModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'codigo_emp_autoriza');
                                                                $distOrigenModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'dist_origen');
                                                                $areaDistOrigModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'area_dist_origen');
                                                                $distDestinoModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'dist_destino');
                                                                $areaDistDestinoModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'area_dist_destino');
                                                                $idBitaCasoModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'id_bita_caso');
                                                                $idCasoModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'id_caso');
                                                                $datosEmpAsignadoModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'datos_empleado_asignado');
                                                                //$ubicacionModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'ubicacion');
                                                                $ubicacionSINOModal = pg_fetch_result($rsBitacoraUbicacion, $varControl, 'ubica_sino');
                                                                ?>
                                                                <th><?php print $fechaUbicacionModal; ?></th>
                                                                <th><?php print $tipoTransModal; ?></th>
                                                                <th><?php print $distOrigenModal; ?></th>
                                                                <th><?php print $areaDistOrigModal; ?></th>
                                                                <th><?php print $distDestinoModal; ?></th>
                                                                <th><?php print $areaDistDestinoModal; ?></th>
                                                                <th><?php print $datosEmpAsignadoModal; ?></th>
                                                                <th><?php print $ubicacionSINOModal; ?></th>
                                                                <th>
                                                                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalUbicaciones<?php print $varControl; ?>" class="modal fade">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                                    <h4 class="modal-title">Detalle de Ubicación de equipo</h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <div class="form-group">
                                                                                         <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Transacción: </label>
                                                                                                <?php print $tipoTransModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Fecha Ubicación: </label>
                                                                                                <?php print $fechaUbicacionModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Detalle de Ubicación: </label>
                                                                                                <?php print $descTransModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Realizado por: </label>
                                                                                                <?php print $empAutorizoModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Distrito Origen: </label>
                                                                                                <?php print $distOrigenModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Area u oficina origen: </label>
                                                                                                <?php print $areaDistOrigModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Distrito Destino: </label>
                                                                                                <?php print $distDestinoModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Area u oficina Destino: </label>
                                                                                                <?php print $areaDistDestinoModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Empleado Asignado: </label>
                                                                                                <?php print $datosEmpAsignadoModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">ID de Caso HelpDesk: </label>
                                                                                                <?php print $idCasoModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">ID de Bitacora Caso HelpDesk: </label>
                                                                                                <?php print $idBitaCasoModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Ubicación Actual: </label>
                                                                                                <?php print $ubicacionSINOModal; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="#myModalUbicaciones<?php print $varControl; ?>" data-toggle="modal" class="btn btn-info">
                                                                    Ver Detalle de Ubicación
                                                                </a>
                                                                </th>
                                                            </tr>
                                                        </tbody>
                                                        <?php 
                                                            }
                                                        
                                                        ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                </section>
                <!--widget end-->
                </div> <!-- div 2-->
                </div>  <!-- div 1-->
            </section>
        </section>


        <!--Finaliza contenido principal-->

        <!--footer start-->
        <footer class="site-footer">
            <div class="text-center">
                2015 &copy; Alcaldia Municipal de San Salvador.
            </div>
        </footer>
        <!--footer end-->
    </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>


    <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
    <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
    <script>
        jQuery(document).ready(function() {
            EditableTable.init();
        });
    </script>

    <script>

        //owl carousel

        $(document).ready(function() {
            $("#owl-demo").owlCarousel({
                navigation: true,
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true,
                autoPlay: true

            });
        });

        //custom select box

        $(function() {
            $('select.styled').customSelect();
        });

    </script>

</body>
</html>
