<!DOCTYPE html>
<html lang="en">
    <?php include '../import_css.php'; ?>
    
    <!--SCRIPT PARA COMBOBOX DEPENDIENTES-->
    <script type="text/javascript" language="javascript" src="../../Resources/js/ajax_cbx.js"></script>	
    <script>
    window.onload=function(){
    from(document.formDetaSoliTraslado.cbxDistrito.value,'divAreaDistrito','cbx_AreaDistrito.php');
    } 
    </script>
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
                    
                     <?php include '../Session.php' ?>
                </div>
                <!--Finaliza logo-->
            </div>
            <!--header end-->

            <!-- Main -->
            <?php include '../main.php'; ?>
            <!-- /End Main -->

            <!--Comienza contenido principal-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row"> <!-- div 1-->
                        <div class="col-lg-12"> <!-- div 2-->
                            <section class="panel">
                                <header class="panel-heading">
                                    <center><h2>DETALLE SOLICITUD DE TRASLADO DE EQUIPO A BODEGA</h2></center>
                                </header>
                                <div class="panel-body"> <!-- div 3-->
                                <form name="formDetaSoliTraslado" action="../../BUSINESS_CAP/AdmonEquipo/ProcDetaSoliTrasladoEquipo.php" method="POST">      
                                    <div class="form-group">
                                    
                               <table  class="table" >
                                      <thead>
                                      <tr>
                                          <th> Id Equipo </th>
                                          <th> Codigo Equipo </th>
                                          <th> Tipo Equipo </th>
                                          <th> Detalle Tipo Equipo</th>
                                          <th> Distrito </th>
                                          <th> Area Distrito </th>
                                          <th> Empleado Asignado </th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                             <?php
                                             include '../../DAO_CAP/AdmonEquipo/variables_asignarEquipo.php';
                                             $idEquipoSeleccionadoMoverBodega = $_SESSION['IdEquipoSolicitudParaMoverBodega'];
                                             $objMoverBodega2 = new SolicitudTrasladoEquipo();
                                             
                                             $rsEqSoliMover2 = $objMoverBodega2->SelectEquiposMoverBodegaSeleccionado($idEquipoSeleccionadoMoverBodega);
                                             
                                             while ($rowEqSoliMovBodega2 = pg_fetch_assoc($rsEqSoliMover2)) { ?>
                                              
                                                <th>
                                                    <?php print $rowEqSoliMovBodega2['id_equipo']?>
                                                    <input name="txtIdEquipoSoliMoverBodega2" value="<?php print $rowEqSoliMovBodega2['id_equipo']?>"
                                                    readonly hidden>
                                                </th>
                                                <th><?php print $rowEqSoliMovBodega2['codigo_equipo']?>
                                                <input name="txtDescArticuloSoliMoverBodega" value="<?php print $rowEqSoliMovBodega2['codigo_equipo']?>"
                                                    readonly hidden>
                                                </th>
                                                <th><?php print $rowEqSoliMovBodega2['m_tipo_equipo']?></th>
                                                <th><?php print $rowEqSoliMovBodega2['tipo_equipo']?></th>
                                                <th><?php print $rowEqSoliMovBodega2['detalle_distrito']?></th>
                                                <th><?php print $rowEqSoliMovBodega2['area']?></th>
                                                <th><?php print $rowEqSoliMovBodega2['empleadoasig']?></th>
                                            </form>
                                          </tr>
                                      </tbody>
                                      <?php  } ?>
                                    </table>
                                   
                              
                                    </div>
                                    <div class="form-group">
                                      <label >Numero de Caso:</label><br>
                                      <p>Si realizas esta solicitud por seguimiento de un caso escribe el # del seguimiento que posee.</p>
                                      <div class="col-sm-3">
                                          <input type="text" name="txtEsCasoSoliBodega" placeholder="# de bitacora de caso" class="form-control" >
                                      </div>
                                    </div><br><br>
                                    <div class="form-group">
                                                <label><h3>Detalle de Traslado</h3></label>
                                           
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <div class="panel-body">
                                                        <input type="text" name="txtDescDetaSoliTrasladoBodega" placeholder="Describa la Solicitado de Traslado" class="form-control" required>
                                                    </div>
                                                </section>
                                            </div>
                                            
                                    </div>
                                    <div class="form-group">
                                        <header class="panel-heading">
                                            <center>
                                            <button type="submit" name="FinalizarSoliTrasladoBodega" class="btn btn-info">Finalizar Solicitud</button>
                                            <button type="button"  class="btn btn-danger">Regresar</button>
                                            </center>                                            
                                        </header>
                                    </div> 
                                    </form>
                                    </div>
                            </section>
                        </div> <!-- div 2-->
                    </div>  <!-- div 1-->
                </section>
            </section>


            <!--Finaliza contenido principal-->

            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    2015 &copy; Alcaldia Municipal de San Salvador.
                </div>
            </footer>
            <!--footer end-->
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>

        <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
        <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
        <script>
                                                    jQuery(document).ready(function() {
                                                        EditableTable.init();
                                                    });
        </script>

        <script>

            //owl carousel

            $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                    navigation: true,
                    slideSpeed: 300,
                    paginationSpeed: 400,
                    singleItem: true,
                    autoPlay: true

                });
            });

            //custom select box

            $(function() {
                $('select.styled').customSelect();
            });
        </script>


    </body>
</html>
