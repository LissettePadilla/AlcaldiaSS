 
<!DOCTYPE html>
<html lang="en">
    <?php include '../import_css.php'; ?>
    
    <!--SCRIPT PARA COMBOBOX DEPENDIENTES-->
    <script type="text/javascript" language="javascript" src="../../Resources/js/ajax_cbx.js"></script>	
    <script>
    window.onload=function(){
    from(document.formDetaSoliTraslado.cbxDistrito.value,'divAreaDistrito','cbx_AreaDistrito.php');
    } 
    </script>
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
                    
                     <?php include '../Session.php' ?>
                </div>
                <!--Finaliza logo-->
            </div>
            <!--header end-->

            <!-- Main -->
            <?php include '../main.php'; ?>
            <!-- /End Main -->

            <!--Comienza contenido principal-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row"> <!-- div 1-->
                        <div class="col-lg-12"> <!-- div 2-->
                            <section class="panel">
                                <header class="panel-heading">
                                    <center><h2>DETALLE SOLICITUD DE TRASLADO DE EQUIPO</h2></center>
                                </header>
                                <div class="panel-body"> <!-- div 3-->
                                <form name="formDetaSoliTraslado" action="../../BUSINESS_CAP/AdmonEquipo/ProcDetaSoliTrasladoEquipo.php" method="POST">      
                                    <div class="form-group">
                                      <label >Numero de Caso:</label><br>
                                      <p>Si realizas esta solicitud por seguimiento de un caso escribe el # del seguimiento que posee.</p>
                                      <div class="col-sm-3">
                                          <input type="text" name="txtEsCasoTras" placeholder="# de bitacora de caso" class="form-control" >
                                      </div>
                                    </div><br><br>
                                    <div class="form-group">
                                                <label><h3>Detalle de Traslado</h3></label>
                                           
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"><h5>Detalle</h5></header>
                                                    <div class="panel-body">
                                                        <input type="text" name="txtDescDetaSoliTraslado" placeholder="Describa la Solicitado de Traslado" class="form-control" required>
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="col-lg-4">
                                                <section class="panel">
                                                    <header class="panel-heading"><h5>Articulo Solicitado</h5></header>
                                                    <div class="panel-body">
                                                        <input type="text" name="txtDescArticuloTraslado" placeholder="Describa el Articulo Solicitado" class="form-control" required>
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="col-lg-4">
                                            <section class="panel">
                                              <header class="panel-heading"><h5>Distrito u oficina de destino</h5></header>
                                              <div class="panel-body">
                                               <select name="cbxDistrito" class="form-control m-bot15" id="" 
                                                          onchange="from(document.formDetaSoliTraslado.cbxDistrito.value,'divAreaDistrito','cbx_AreaDistrito.php')">
                                                  <?php 
                                                  include '../../DAO_CAP/Conexion/admon_conexion.php';
                                                  $queryDistritoT = "SELECT ID_DISTRITO,DETALLE_DISTRITO FROM DISTRITO;";
                                                  $rsDistritoT = pg_query($queryDistritoT); 
                                                  while ($Dist2 = pg_fetch_array($rsDistritoT)) {
                                                  ?>
                                                      <option value="<?php echo $Dist2['id_distrito'] ?>"><?php echo $Dist2['detalle_distrito'] ?></option>   
                                                  <?php
                                                  }
                                                  ?>
                                              </select>
                                              </div>
                                            </section>
                                         </div>
                                         <div class="col-lg-4">
                                            <section class="panel">
                                              <header class="panel-heading"><h5>Area del Distrito u oficina</h5></header>
                                              <div class="panel-body" id="divAreaDistrito">

                                              </div>
                                            </section>
                                         </div>
                                    </div>
                                    <div class="form-group">
                                        <header class="panel-heading">
                                            <center>
                                            <button type="submit" name="FinalizarSoliTraslado" class="btn btn-info">Finalizar Solicitud</button>
                                            <button type="button"  class="btn btn-danger">Regresar</button>
                                            </center>                                            
                                        </header>
                                    </div> 
                                    </form>
                                    </div>
                            </section>
                        </div> <!-- div 2-->
                    </div>  <!-- div 1-->
                </section>
            </section>


            <!--Finaliza contenido principal-->

            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    2015 &copy; Alcaldia Municipal de San Salvador.
                </div>
            </footer>
            <!--footer end-->
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>

        <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
        <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
        <script>
                                                    jQuery(document).ready(function() {
                                                        EditableTable.init();
                                                    });
        </script>

        <script>

            //owl carousel

            $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                    navigation: true,
                    slideSpeed: 300,
                    paginationSpeed: 400,
                    singleItem: true,
                    autoPlay: true

                });
            });

            //custom select box

            $(function() {
                $('select.styled').customSelect();
            });
        </script>


    </body>
</html>
