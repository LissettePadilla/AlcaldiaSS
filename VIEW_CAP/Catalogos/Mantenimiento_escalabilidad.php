
<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->


      <!--Comienza contenido principal-->
      <!--*********************************************************************** -->
         <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <!-- inicio de pagina-->
          <section class="panel">
            <header class="panel-heading">
              Escalabilidad de Casos
            </header>
            <div class="panel-body">
              <div class="adv-table editable-table ">
                <div class="space15"></div>
                <?php include '../../DAO_CAP/HelpDesk/escalabilidad_class.php';

                $db = new Escalabilidad();
                $response = $db->updateEscalabilidad();
                echo $response;
                ?>
                
              </div>
            </div>
          </section>
            <!-- finaliza pag-->
        </section>
      </section>
      <!--main content end-->
      <!-- ********************************************************************** -->
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </footer>
      <!--footer end-->
    </section>
      <?php include '../import_js.php';?>
  </body>
</html>
