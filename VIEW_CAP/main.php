<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
            
          <li>
                  <a href="../AdmonEquipo/index.php">
                      <i class="icon-bar-chart"></i>
                      <span>Inicio</span>
                  </a>
          </li>

          <li class="sub-menu">
              <a  href="javascript:;" >
                  <i class="icon-book"></i>
                  <span>Solicitudes de Compra</span>
              </a>
              <ul class="sub">
                  <li><a  href="../AdmonEquipo/SolicitudCompra.php">Crear Nueva Solicitud</a></li>
                  <li class="sub-menu">
                      <a  href="../AdmonEquipo/VerSolicitudesCompra.php">Ver Solicitudes</a>
                  </li>
              </ul>
          </li>

          <li class="sub-menu">
              <a href="javascript:;" >
                  <i class="icon-shopping-cart"></i>
                  <span>Compras</span>
              </a>
              <ul class="sub">
                  <li><a  href="../AdmonEquipo/PreRegistrarCompra.php">Registrar Compra</a>
                  <li class="sub-menu">
                      <a  href="../AdmonEquipo/VerCompras.php">Ver Compras</a>
                  </li>
              </ul>
          </li>

          
            
          <!-- * * * HELP DESK * * * -->
          <li class="sub-menu">
            <a href="javascript:;" >
              <i class="icon-table"></i>
                <span>Casos HelpDesk</span>
            </a>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/ingresoCaso.php">Crear Caso</a>
              </li>
            </ul>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/revision_casos.php">Revisión de Casos</a>
              </li>
            </ul>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/vista_casos_tenicos.php">Mis Casos Asignados</a>
                  <ul class="sub">
                    <li>
                      <a  href="../HelpDesk/Gestion_casos.php">Seguimientos de Casos</a>
                    </li>
                  </ul>
              </li>
            </ul>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="javascript:;">Casos Recientes</a>
              </li>
            </ul>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/asignacion_casos.php">Asignar Caso</a>
              </li>
            </ul>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="javascript:;">Estados de Casos</a>
                  <ul class="sub">
                    <li>
                      <!--<a  href="javascript:;">option_</a>-->
                    </li>
                  </ul>
              </li>
            </ul>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/cierre_casos.php">Cierre de Casos</a>
              </li>
            </ul>
          </li>
          <!-- * * * END HELP DESK * * * -->

          <!-- * * * TECNICOS * * * -->
          <li class="sub-menu">
              <a href="javascript:;" >
                <i class="icon-user"></i>
                  <span>Técnicos</span>
              </a>
              <ul class="sub">
                <li class="sub-menu">
                  <a  href="../HelpDesk/ingresoTecnico.php">Agregar Técnico</a>
                </li>
              </ul>
              <ul class="sub">
                <li class="sub-menu">
                  <a  href="../HelpDesk/modificar_tecnico.php">Modificar Datos</a>
                </li>
              </ul>
              <ul class="sub">
                <li class="sub-menu">
                  <a  href="../HelpDesk/reset_password.php">Reset Password</a>
                </li>
              </ul>
          </li>
          <!-- * * * END TECNICOS * * * -->

          <!-- * * * EQUIPOS TECNOLOGICOS * * * -->

          <li class="sub-menu">
                      <a class="" href="javascript:;" >
                          <i class="icon-desktop"></i>
                          <span>Equipos tecnológicos</span>
                      </a>
                      <ul class="sub">
                          <li>
                              <a  href="../AdmonEquipo/IngresarEquipoTec.php">Ingresar un Equipo</a>
                          </li>
                          <li class="sub-menu">
                              <a href="javascript:;">Mantenimiento de Equipos</a>
                          
                          <ul class="sub">
                              <a  href="../AdmonEquipo/IngresarSoftwareEquipo.php">Ingresar Software a Equipo</a>
                          </ul>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/RelacionarEquipos.php">Relacionar Equipos</a>
                          </ul>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/VerEquiposTec.php">Ver Equipos</a>
                          </ul>
                              </li>
                      </ul>          
          </li>
           <!-- * * * TRANSACCIONES EQUIPO * * * -->
           <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-truck"></i>
                          <span>Transacciones de Equipo</span>
                      </a>
                      <ul class="sub">
                          <!--
                          <li>
                              <a  href="../AdmonEquipo/TransAsignacionEq.php">Asignar un Equipo</a>
                          </li>
                          -->
                          <li class="sub-menu">
                              <a  href="javascript:;">Solicitud de Traslado de Equipo</a>
                              <ul class="sub">
                                 <li><a  href="../AdmonEquipo/SolicitudTrasladoEquipo.php">Asignar un Equipo</a></li>
                              </ul>
                              <ul class="sub">
                                 <li><a  href="../AdmonEquipo/SolicitudTrasladoEquipoBodega.php">Mover a Bodega</a></li>
                              </ul>
                          </li>
                          <li class="sub-menu">
                              <a  href="../AdmonEquipo/VerSolicitudTrasladoEquipo.php">Ver Solicitudes de Traslado</a>
                          </li>
                          <!--
                          <li class="sub-menu">
                              <a  href="../AdmonEquipo/TransReAsignacionEq.php">Mover un Equipo</a>
                          </li>
                          -->
                      </ul>           
           </li>

            <!-- * * * CATALOGOS * * * -->
          <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-gears"></i>
                          <span>Mantenimiento de Catalogos</span>
                      </a>
                      <ul class="sub">
                              <li class="sub-menu">
                                      <a  href="javascript:;">Solicitud de Compra</a>
                                      <ul class="sub">
                                          <li><a  href="../catalogos/Mantenimiento_Estado_Compra.php">Estado</a></li>
                                      </ul>
                              </li>
                              <li class="sub-menu">
                                      <a  href="javascript:;">Compra</a>
                                      <ul class="sub">
                                          <li><a  href="../catalogos/Mantenimiento_Tipo_doc.php">Tipo Documento</a></li>
                                      </ul>
                              </li>
                              <li class="sub-menu">
                                      <a  href="javascript:;">Transacciones de Equipo</a>
                                      <ul class="sub">
                                          <li><a  href="../catalogos/Mantenimiento_Tipo_trans.php">Tipo Transacción</a></li>
                                      </ul>
                              </li>
                              <li class="sub-menu"> <a href="">Equipos Tecnológicos</a>
                                <ul class="sub">
                                  <li><a  href="../catalogos/Mantenimiento_Tipo_Equipo.php">Tipo de Equipo</a></li>
                                  <li><a  href="../catalogos/Mantenimiento_Estado_Equipo.php">Estado de Equipo</a></li>
                                  <li><a  href="../catalogos/Mantenimiento_Tipo_Adq.php">Tipo de Adquisición</a></li>
                                  <li><a  href="../catalogos/Mantenimiento_Marca_Equipo.php">Marca de Equipos</a></li>
                                  <li><a  href="../catalogos/Mantenimiento_Modelo_Equipo.php">Modelo de Equipos</a></li>
                                  <li class="sub-menu">
                                      <a >Software</a>
                                      <ul class="sub">
                                          <li><a  href="../catalogos/Mantenimiento_software.php">Detalles</a></li>
                                          <li><a  href="../catalogos/Mantenimiento_Tipo_Software.php">Tipo Software</a></li>
                                      </ul>
                                  </li>
                              </ul>
                            </li>
                            <li class="sub-menu"> <a href="">Help-Desk</a>
                                <ul class="sub">
                                  <li><a  href="../catalogos/estado_caso.php">Estado de Caso</a></li>
                                  <li><a  href="../catalogos/escalabilidad.php">Escalabilidad</a></li>
                                  <li><a  href="../catalogos/tipo_caso.php">Tipo Caso</a></li>
                              </ul>
                            </li>
                             <li class="sub-menu"> <a href="">Empleados</a>
                                <ul class="sub">
                                  <li><a  href="../catalogos/estado_empleado.php">Estado Empleado</a></li>
                                  <li><a  href="../catalogos/acceso_empleado.php">Accesos de Empleado</a></li>
                                  <li><a  href="../catalogos/empleado_subsistema.php">Empleado_Subsistema</a></li>
                                  <li><a  href="../catalogos/permisos.php">Permisos de Empleado</a></li>
                                  <li><a  href="../catalogos/rol.php">Rol</a></li>
                                  <li><a  href="../catalogos/usuario_permiso.php">Usuario Permiso</a></li>
                              </ul>
                            </li>
                            <li class="sub-menu"> <a href="">Sub-sistemas</a>
                                <ul class="sub">
                                  <li><a  href="../catalogos/codigo_subsistema.php">Sub-Sistemas</a></li>
                              </ul>
                            </li>
                        </li>
                      </ul>
          </li>
          <!-- * * * END CATALOGOS * * * -->
          <li class="sub-menu">
              <a  href="javascript:;" >
                  <i class="icon-book"></i>
                  <span>Reportes</span>
              </a>
              <ul class="sub">
                  <li><a  href="../HelpDesk/reportes_help_desk.php">Reporte Help Desk</a></li>
                  <li class="sub-menu">
                      <a  href="../HelpDesk/reportes_adm_equipo.php"> Administracion Equipo</a>
                  </li>
              </ul>
          </li>
          <li class="sub-menu">
              <a  href="../index.html" >
                  <i class="icon-book"></i>
                  <span>Ir a Carnetizacion</span>
              </a>
          </li>

          <!--multi level menu end-->
          </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

</body>
</html>		