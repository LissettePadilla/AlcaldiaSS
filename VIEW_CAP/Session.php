<!DOCTYPE html>
<html>
<head>
        <?php
        session_start(); 
        if ($_SESSION['IngresoSistema'] == NULL){
           header( 'Location: ../../index.php' ) ;
        } 
        ?>
</head>
<body>

 <div class="top-nav ">
              <ul class="nav pull-right top-menu">
                  <!-- user login dropdown start-->
                  <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                          <span class="username">
                          <?php 
                          echo "Bienvenido:<br/>";               
                          echo $_SESSION['NombreEmpIngresoSistema'];
                          ?>
                          </span>
                          <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu extended logout">
                          <div class="log-arrow-up"></div>
                          <li><a href="#"><i class=" icon-suitcase"></i>Perfil</a></li>
                          <li><a href="#"><i class="icon-cog"></i> Editar Info.</a></li>
                          <li><a href="#"><i class="icon-bell-alt"></i> Notificaciones</a></li>
                          <li><a href="../LogOut.php"><i class="icon-key"></i> Cerrar Sesión </a></li>
                      </ul>
                  </li>
                  <!-- user login dropdown end -->
              </ul>

          </div>

</body>
</html>	