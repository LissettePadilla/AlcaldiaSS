<?php

//
include_once("../../DAO_CAP/Connection/Connection.class.php");

class Asignacion_caso{

	private $id_solicitud_caso;
	private $descripcion_caso;
	private $fecha_caso;
	private $id_deta_empleado_solicitante;
	private $id_estado_caso;
	private $id_bitacora_caso;
	private $descripcion_seguimiento;
	private $id_tipo_caso;
	private $id_deta_empleado_tecnico;
	private $id_escalabilidad;
	private $id_equipo_tecnologico;
	private $id_deta_empleado_crea;

	public function getId_solicitud_caso(){
		return $this->id_solicitud_caso;
	}

	public function setId_solicitud_caso($id_solicitud_caso){
		$this->id_solicitud_caso = $id_solicitud_caso;
	}

	public function getDescripcion_caso(){
		return $this->descripcion_caso;
	}

	public function setDescripcion_caso($descripcion_caso){
		$this->descripcion_caso = $descripcion_caso;
	}

	public function getFecha_caso(){
		return $this->fecha_caso;
	}

	public function setFecha_caso($fecha_caso){
		$this->fecha_caso = $fecha_caso;
	}

	public function getId_deta_empleado_solicitante(){
		return $this->id_deta_empleado_solicitante;
	}

	public function setId_deta_empleado_solicitante($id_deta_empleado_solicitante){
		$this->id_deta_empleado_solicitante = $id_deta_empleado_solicitante;
	}

	public function getId_estado_caso(){
		return $this->id_estado_caso;
	}

	public function setId_estado_caso($id_estado_caso){
		$this->id_estado_caso = $id_estado_caso;
	}

	public function getId_bitacora_caso(){
		return $this->id_bitacora_caso;
	}

	public function setId_bitacora_caso($id_bitacora_caso){
		$this->id_bitacora_caso = $id_bitacora_caso;
	}

	public function getdescripcion_seguimiento(){
		return $this->descripcion_seguimiento;
	}

	public function setdescripcion_seguimiento($descripcion_seguimiento){
		$this->descripcion_seguimiento = $descripcion_seguimiento;
	}

	public function getid_tipo_caso(){
		return $this->id_tipo_caso;
	}

	public function setid_tipo_caso($id_tipo_caso){
		$this->id_tipo_caso = $id_tipo_caso;
	}

	public function getId_deta_empleado_tecnico(){
		return $this->id_deta_empleado_tecnico;
	}

	public function setId_deta_empleado_tecnico($id_deta_empleado_tecnico){
		$this->id_deta_empleado_tecnico = $id_deta_empleado_tecnico;
	}

	public function getId_escalabilidad(){
		return $this->id_escalabilidad;
	}

	public function setId_escalabilidad($id_escalabilidad){
		$this->id_escalabilidad = $id_escalabilidad;
	}

	public function getId_equipo_tecnologico(){
		return $this->id_equipo_tecnologico;
	}

	public function setId_equipo_tecnologico($id_equipo_tecnologico){
		$this->id_equipo_tecnologico = $id_equipo_tecnologico;
	}

	public function getId_deta_empleado_crea(){
		return $this->id_deta_empleado_crea;
	}

	public function setId_deta_empleado_crea($id_deta_empleado_crea){
		$this->id_deta_empleado_crea = $id_deta_empleado_crea;
	}

	//--------------------------------------------

	function consultar_caso(){

		$fcnid_solicitud_caso = $this -> getId_solicitud_caso();
		$select = "SELECT * FROM solicitud_caso
		INNER JOIN detalle_empleado ON solicitud_caso.id_deta_empleado_solicitante= detalle_empleado.id_deta_empleado
		INNER JOIN estado_caso ON solicitud_caso.id_estado_caso= estado_caso.id_estado_caso
		WHERE id_estado_caso= '$fcnid_solicitud_caso'  ORDER BY fecha_caso ASC";

		$result = pg_query($con, $select);

		echo "Fetch row: <br>";

		while ($row = pg_fetch_row($result)) {
		print_r($row[1]);

		pg_close($con);
		}
	}

	function insertBitacora_caso(){
		$fcndescripcion_seguimiento = $this -> getdescripcion_seguimiento();
		$fcnid_solicitud_caso = $this -> getId_solicitud_caso();
		$fcnid_tipo_caso = $this -> getid_tipo_caso();
		$fcnid_deta_empleado_tecnico = $this -> getId_deta_empleado_tecnico();
		$fcnid_escalabilidad = $this -> getId_escalabilidad();
		$insert = "INSERT INTO bitacora_caso (id_bitacora_caso, descripcion_seguimiento, fecha_seguimiento. id_solicitud_caso, id_tipo_caso, id_deta_empleado_tecnico, id_escalabilidad, id_equipo_tecnologico, id_deta_empleado_crea)
		VALUES ('', '$fcndescripcion_seguimiento' , NOW(), '$fcnid_solicitud_caso', '$fcnid_tipo_caso' , '$fcnid_deta_empleado_tecnico' , '$fcnid_escalabilidad', '', '')";

		$result = pg_query($con, $insert);

		if (!$result) {
			echo pg_last_error($con);
		}else{
			echo "Caso Asignado con exito";
		}
		pg_close($con);
	}

	function updateCasos_asignar(){
		$fcnid_solicitud_caso = $this -> setId_solicitud_caso();
		$update = "UPDATE solicitud_caso SET id_estado_caso= '2' WHERE id_solicitud_caso = '$fcnid_solicitud_caso'";

		$result = pg_query($con, $update);
		if (!$result) {
			echo pg_last_error($con);
		}
		pg_close($con);
	}

}

 ?>
