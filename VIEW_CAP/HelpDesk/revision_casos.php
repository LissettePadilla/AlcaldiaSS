
<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

    <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->


        <!--Comienza contenido principal-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading">
                  Casos por Asignar
                </header>
                <div class="panel-body">
                  <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="asignacion">
                      <thead>
                        <tr>
                          <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                          <th>Descripcion de Caso</th> <!-- descripcion_caso-->
                          <th>Fecha de Creación</th> <!-- fecha_caso -->
                          <th>Solicitante</th> <!-- id_deta_persona-->
                          <th>Estado del Caso</th> <!-- id_estado_caso-->
                          <th>Asignacion de Caso</th> <!-- boton para asignar caso -->
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>100123</td>
                          <td>El Computador queda con pantalla negro y da mensaje en letra blanca</td>
                          <td>25/07/2015</td>
                          <td>Rodrigo Vaquerano</td>
                          <td>Pendiente</td>
                          <td>
                            <center>
                              <a class="edit" href="asignacion_casos.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Asignar
                                </button>
                              </a>
                          </td>
                        <tr>
                          <td>100124</td>
                          <td>No me conecta a internet no puedo entrar a facebook</td>
                          <td>26/07/2015</td>
                          <td>Thania Figueroa</td>
                          <td>Pendiente</td>
                          <td>
                            <center>
                              <a class="edit" href="asignacion_casos.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Asignar
                                </button>
                              </a>
                          </td>
                        </tr>
                        <tr>
                          <td>100123</td>
                          <td>Problemas con el router del distrito</td>
                          <td>27/07/2015</td>
                          <td>Mauricio Arias</td>
                          <td>Pendiente</td>
                          <td>
                            <center>
                              <a class="edit" href="asignacion_casos.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Asignar
                                </button>
                              </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </section>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading">
                  Casos pendientes de Cierre
                </header>
                <div class="panel-body">
                  <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="finalizar">
                      <thead>
                        <tr>
                          <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                          <th>Descripcion de Caso</th> <!-- descripcion_caso-->
                          <th>Fecha de Seguimiento</th> <!-- fecha_seguimiento -->
                          <th>Tecnico Asignado</th> <!-- id_deta_persona_tecnico-->
                          <th>Estado del Caso</th> <!-- id_estado_caso-->
                          <th>Asignacion de Caso</th> <!-- boton para asignar caso -->
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>100123</td>
                          <td>Reemplazo de Disco duro y reinstalacion de sistema operativo y programas</td>
                          <td>25/07/2015</td>
                          <td>Carlos Arias</td>
                          <td>Completado</td>
                          <td>
                            <center>
                              <a class="edit" href="historial_bitacora_usuario.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Historial
                                </button>
                              </a>
                          </td>
                        <tr>
                          <td>100124</td>
                          <td>Controlador de red daba problemas, se reinstala controlador de red</td>
                          <td>26/07/2015</td>
                          <td>Juan Lemus</td>
                          <td>Completado</td>
                          <td>
                            <center>
                              <a class="edit" href="historial_bitacora_usuario.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Historial
                                </button>
                              </a>
                          </td>
                        </tr>
                        <tr>
                          <td>100125</td>
                          <td>Se reemplaza router del distrito 5 debido a desperfectos</td>
                          <td>27/07/2015</td>
                          <td>Karla Hernandez</td>
                          <td>Completado</td>
                          <td>
                            <center>
                              <a class="edit" href="historial_bitacora_usuario.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Historial
                                </button>
                              </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </section>
            </div>
          </div>
            <!-- page end-->
        </section>
      </section>
        <!--main content end-->
        <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
        <!--footer end-->
    </section>

    <?php include '../import_js.php';?>
  </body>
</html>
