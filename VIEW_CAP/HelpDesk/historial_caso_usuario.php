
<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->

      <!--Comienza contenido principal-->

      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Historial de Casos de Usuario
                          </header>
                          <div class="panel-body">
                                <div class="adv-table">
                                    <table  class="display table table-bordered table-striped" id="example">
                                      <thead>
                                      <tr>
                                          <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                                          <th>Fecha de Creación</th> <!-- fecha_caso -->
                                          <th>Descripcion de Caso</th> <!-- descripcion_seguimiento-->
                                          <th>Fecha de Seguimiento</th> <!-- fecha_seguimiento-->
                                          <th>Estado del Caso</th> <!-- id_estado_caso-->
                                          <th>Codigo de Equipo</th> <!-- id_equipo_tecnologico-->
                                          <th>Tecnico Asignado</th> <!-- id_deta_persona_tecnico -->
                                          <th>Bitacora de Caso</th> <!-- boton para ver historial de cada caso -->
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td>100123</td>
                                          <td>25/07/2015</td>
                                          <td>Reemplazo de Disco duro y reinstalacion de sistema operativo y programas</td>
                                          <td> 29/07/2015 </td>
                                          <td>Completado</td>
                                          <td>125215</td>
                                          <td>Carlos Arias</td>
                                          <td>
                                          <center>
                                            <a class="edit" href="historial_bitacora_usuario.php">
                                               <button class="btn btn-primary btn-sm btn-block">
                                                <i class="icon-file"></i>&nbsp&nbspHistorial
                                              </button>
                                            </a>
                                          </td>
                                      <tr>
                                          <td>100153</td>
                                          <td>30/07/2015</td>
                                          <td>Pantalla dañada, se reemplaza monitor</td>
                                          <td> 08/08/2015 </td>
                                          <td>Completado</td>
                                          <td>125215</td>
                                          <td>Juan Lemus</td>
                                          <td>
                                          <center>
                                            <a class="edit" href="historial_bitacora_usuario.php">
                                               <button class="btn btn-primary btn-sm btn-block">
                                                <i class="icon-file"></i>&nbsp&nbspHistorial
                                              </button>
                                            </a>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>100220</td>
                                          <td>12/08/2015</td>
                                          <td>Problema de controlador de red, controlador reinstalado </td>
                                          <td> 13/08/2015 </td>
                                          <td>Completado</td>
                                          <td>125215</td>
                                          <td>Karla Hernandez</td>
                                          <td>
                                          <center>
                                            <a class="edit" href="historial_bitacora_usuario.php">
                                               <button class="btn btn-primary btn-sm btn-block">
                                                <i class="icon-file"></i>&nbsp&nbspHistorial
                                              </button>
                                            </a>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>100321</td>
                                          <td>16/08/2015</td>
                                          <td>Problemas con facebook, caso cancelado </td>
                                          <td> 16/08/2015 </td>
                                          <td>Denegado</td>
                                          <td>125215</td>
                                          <td></td>
                                          <td>
                                          <center>
                                            <a class="edit" href="historial_bitacora_usuario.php">
                                               <button class="btn btn-primary btn-sm btn-block">
                                                <i class="icon-file"></i>&nbsp&nbspHistorial
                                              </button>
                                            </a>
                                          </td>
                                      </tr>
                                      </tbody>
                          </table>
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
          <!--footer start-->
      <div class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </div>
      <!--footer end-->
  </section>

    <?php include '../import_js.php';?>
  </body>
</html>
