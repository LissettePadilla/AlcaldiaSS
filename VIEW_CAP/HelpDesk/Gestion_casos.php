<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

    <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->


        <!--Comienza contenido principal-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading">
                  Gestion de casos
                </header>

                <div class="panel-body">
                  <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="asignacion">
                      <thead>
                        <tr>
                          <th>Codigo de Empleado</th> <!-- codigo_empleado -->
                          <th>Tecnico asignado</th> <!-- usuario_deta_persona-->
                          <th>Estado de empleado</th>
                          <th>Casos asignados</th> <!-- id_deta_persona_asignada -->


                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>100</td>
                          <td>Ronald Arias</td>
                          <th>Activo</th>
                          <td>
                            <center>
                              <a class="edit" href="historial_bitacora_tecnico.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Revisar
                                </button>
                              </a>
                          </td>
                        <tr>
                          <td>101</td>
                          <td>Ricardo Lopez</td>
                          <th>Activo</th>
                          <td>
                            <center>
                              <a class="edit" href="historial_bitacora_tecnico.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Revisar
                                </button>
                              </a>
                          </td>
                        </tr>
                        <tr>
                          <td>102</td>
                          <td>Osvaldo Serrano</td>
                          <th>Activo</th>
                          <td>
                            <center>
                              <a class="edit" href="historial_bitacora_tecnico.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Revisar
                                </button>
                              </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </section>
            </div>
          </div>

            <!-- page end-->
        </section>
      </section>
        <!--main content end-->
        <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
        <!--footer end-->
    </section>

    <?php include '../import_js.php';?>
  </body>
</html>
