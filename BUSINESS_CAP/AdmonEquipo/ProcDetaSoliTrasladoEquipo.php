<?php

include ("../../DAO_CAP/AdmonEquipo/variables_asignarEquipo.php");
session_start();

if (isset($_POST['FinalizarSoliTraslado'])){
    
    $descMan = $_POST['txtDescDetaSoliTraslado'];
    $detaEqMan = $_POST['txtDescArticuloTraslado'];
    $areaDistMan = $_POST['cbxAreaDistrito'];
    $empCreaSolicitudMan = $_SESSION['IngresoSistema'];
    $esCasoMan = $_POST['txtEsCasoTras'];
    
    $objSolicitudTrasladoEquipo = new SolicitudTrasladoEquipo();
    $objSolicitudTrasladoEquipo->setDescSoliTraslado($descMan);
    $objSolicitudTrasladoEquipo->setEquipoTraslado($detaEqMan);
    $objSolicitudTrasladoEquipo->setIdAreaDist($areaDistMan);
    $objSolicitudTrasladoEquipo->setIdEmpCreaSoliTras($empCreaSolicitudMan);
    
    if ($esCasoMan!=''){
        
        $objSolicitudTrasladoEquipo->setIdCaso($esCasoMan);
        
        $ExisteIdCaso = $objSolicitudTrasladoEquipo->ValidarIdCaso();
    
        if($ExisteIdCaso!=NULL){
           $objSolicitudTrasladoEquipo->IngresarSolicitudTraslado();

                 echo "<script>";
                 echo "alert('Solicitud Enviada Satisfactoriamente');";  
                 echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
                 echo "</script>";
            
        } else {
            echo "<script>";
            echo "alert('El Id del seguimiento no es valido..., favor verifique si el caso esta Abierto');";  
            echo "window.location = '../../VIEW_CAP/AdmonEquipo/SolicitudTrasladoEquipo.php';";
            echo "</script>";
                }
    } else {
        $esCasoMan = 'NULL';
        $objSolicitudTrasladoEquipo->setIdCaso($esCasoMan);
        
        $objSolicitudTrasladoEquipo->IngresarSolicitudTraslado();
    
                echo "<script>";
                 echo "alert('Solicitud Enviada Satisfactoriamente');";  
                 echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
                 echo "</script>";
        
    }  
}

if (isset($_POST['FinalizarSoliTrasladoBodega'])){
    
    $descBodegaMan = $_POST['txtDescDetaSoliTrasladoBodega'];
    $descEquipoBodegaMan = $_POST['txtDescArticuloSoliMoverBodega'];
    $empSolicitaEquipoBodega = $_SESSION['IngresoSistema'];
    $idEquipoSelecMoverBodega = $_POST['txtIdEquipoSoliMoverBodega2'];
    $idCasoSoliTrasladoBodega = $_POST['txtEsCasoSoliBodega'];
    
    $objSolicitudEquipoBodega = new SolicitudTrasladoEquipo();
    $objSolicitudEquipoBodega->setDescSoliTraslado($descBodegaMan);
    $objSolicitudEquipoBodega->setEquipoTraslado($descEquipoBodegaMan);
    $objSolicitudEquipoBodega->setIdEmpCreaSoliTras($empSolicitaEquipoBodega);
    $objSolicitudEquipoBodega->setIdEquipoSeleccionado($idEquipoSelecMoverBodega);
    
    if ($idCasoSoliTrasladoBodega!=''){
        
        $objSolicitudEquipoBodega->setIdCaso($idCasoSoliTrasladoBodega);
        
        $ExisteIdCaso = $objSolicitudEquipoBodega->ValidarIdCaso();
    
        if($ExisteIdCaso!=NULL){
           $objSolicitudEquipoBodega->IngresarSolicitudTrasladoBodega();

                 echo "<script>";
                 echo "alert('Solicitud Enviada Satisfactoriamente');";  
                 echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
                 echo "</script>";
            
        } else {
            echo "<script>";
            echo "alert('El Id del seguimiento no es valido..., favor verifique si el caso esta Abierto');";  
            echo "window.location = '../../VIEW_CAP/AdmonEquipo/SolicitudTrasladoEquipoBodegaDeta.php';";
            echo "</script>";
                }
    } else {
        $idCasoSoliTrasladoBodega = 'NULL';
        $objSolicitudEquipoBodega->setIdCaso($idCasoSoliTrasladoBodega);
        
        $objSolicitudEquipoBodega->IngresarSolicitudTrasladoBodega();
    
                echo "<script>";
                 echo "alert('Solicitud Enviada Satisfactoriamente');";  
                 echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
                 echo "</script>";
        
    } 
    
}
