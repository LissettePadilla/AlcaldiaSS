<?php
include ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

if(isset($_POST['EnviarSoliCompra'])){
    $DescEncaSoliMan = $_POST['txtDescripcionSoliCompra'];
    $EsCasoIdMan = $_POST['txtEsCaso'];
    
    session_start();
    
    $idEmpleadoSolicitanteMan = $_SESSION['IngresoSistema'];//ID DEL LOGUEADO
    
    $objEncaSoliCompra = new EncaSolicitudCompra();
    
    $objEncaSoliCompra->setDescSoli($DescEncaSoliMan);
    $objEncaSoliCompra->setIdEmpSolicitante($idEmpleadoSolicitanteMan);
    
    if ($EsCasoIdMan!=''){
        
        $objEncaSoliCompra->setIdCaso($EsCasoIdMan);
        
        $ExisteIdCaso = $objEncaSoliCompra->ValidarIdCaso();
    
        if($ExisteIdCaso!=NULL){
            $idSoliCompraInsertada = $objEncaSoliCompra->IngresarEncaSolicitudCompra();
    
            if($idSoliCompraInsertada!=NULL){
            /*
             Se inicia una session con el id del encabezado para llevarlo
             a la pagina donde realizaremos el detalle.
             */
            $_SESSION['IdSoliCompra']=$idSoliCompraInsertada;

            header( 'Location: ../../VIEW_CAP/AdmonEquipo/SolicitudDetaCompra.php' ) ; 
            }else{
                 echo "<script>";
                 echo "alert('Error en la Solicitud');";  
                 echo "window.location = '../../VIEW_CAP/AdmonEquipo/SolicitudCompra.php';";
                 echo "</script>";
            }
        } else {
            echo "<script>";
            echo "alert('El Id del seguimiento no es valido..., favor verifique si el caso esta Abierto');";  
            echo "window.location = '../../VIEW_CAP/AdmonEquipo/SolicitudCompra.php';";
            echo "</script>";
                }
    } else {
        $EsCasoIdMan = 'NULL';
        $objEncaSoliCompra->setIdCaso($EsCasoIdMan);
        
        $idSoliCompraInsertada = $objEncaSoliCompra->IngresarEncaSolicitudCompra();
    
        if($idSoliCompraInsertada!=NULL){
    
        /*
         Se inicia una session con el id del encabezado para llevarlo
         a la pagina donde realizaremos el detalle.
         */
        $_SESSION['IdSoliCompra']=$idSoliCompraInsertada;

        header( 'Location: ../../VIEW_CAP/AdmonEquipo/SolicitudDetaCompra.php') ; 
        }else{
             echo "<script>";
             echo "alert('Error en la Solicitud');";  
             echo "window.location = '../../VIEW_CAP/AdmonEquipo/SolicitudCompra.php';";
             echo "</script>";
        }
    }   
}

?>

