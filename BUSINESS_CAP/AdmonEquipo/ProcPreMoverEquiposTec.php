<?php
    session_start();
    
if (isset($_POST['GenerarMovimiento'])){
    
    $_SESSION['IdSolicitudTrasladoParaProcesar'] = $_POST['txtIdSoliTras'];
    
    //echo $_SESSION['IdSolicitudTrasladoParaProcesar'];
    echo "<script>";  
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/TransReAsignacionEq.php';";
    echo "</script>";
    
}

if (isset($_POST['DenegarSolicitud'])){
    
    require_once ("../../DAO_CAP/AdmonEquipo/variables_asignarEquipo.php");
    
    $IdEmpleadoDenegar = $_SESSION['IngresoSistema'];
    $idSolicitudTrasDenegar = $_POST['txtIdSoliTras'];
    
    $objDenegarSolicitudTraslado = new SolicitudTrasladoEquipo();
    $objDenegarSolicitudTraslado->setIdEmpleadoDenegaAprueba($IdEmpleadoDenegar);
    $objDenegarSolicitudTraslado->setIdSolicitudDenegar($idSolicitudTrasDenegar);
    $objDenegarSolicitudTraslado->DenegarSoliTraslado();
    
    echo "<script>";
    echo "alert('Solicitud de Traslado Denegada Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerSolicitudTrasladoEquipo.php';";
    echo "</script>";
    
}

