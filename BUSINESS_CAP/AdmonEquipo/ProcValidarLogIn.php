<?php
include("../../DAO_CAP/AdmonEquipo/variables_login.php");

if(isset($_POST['ValidaLogin'])){
    $usuarioMan = $_POST['txtUsuario'];
    $contraMan = $_POST['txtContra'];
    
    $objLogIn = new Login();
    $objLogIn->setUsuario($usuarioMan);
    $objLogIn->setContra($contraMan);
    
    $resultado = $objLogIn->ValidarLogin(); //ID DEL LOGUEADO
    
    if ($resultado!=NULL){
        
        $objAccesoEmp = new AccesoEmpleado();
        $objAccesoEmp->setIdUsuario($resultado);
        $objAccesoEmp->InsertarAccesoEmpleado();
        
        $objEmpleado = new NombreEmpleadoLogeado();
        $objEmpleado->setIdEmpleado($resultado);
        $nombreEmpleado = $objEmpleado->RetornarNombreUsuarioLogeado();
        
        // ----------- Iniciamos sesion ------------
        session_start();
        $_SESSION['IngresoSistema']=$resultado;
        $_SESSION['NombreEmpIngresoSistema']=$nombreEmpleado;
        //--------------------------------------------
        
        header( 'Location: ../../VIEW_CAP/AdmonEquipo/index.php' ) ;
        
         
    } else {
            echo "<script>";
            echo "alert('Ingrese datos correctos');";  
            echo "window.location = '../../index.php';";
            echo "</script>";
    }
}

