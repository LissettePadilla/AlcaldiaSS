<?php

  include("../../DAO_CAP/HelpDesk/model.detalleEmpleado.php");

  $id=$_POST['id_deta_empleado'];
  $User=$_POST['usuario_deta_persona'];
  $Pass=$_POST['pass_deta_persona'];
  $codEmpleado=$_POST['codigo_empleado'];
  $idPersona=$_POST['id_persona'];
  $idEstadoEmpleado=$_POST['id_estado_empleado'];
  $idAreaDistrito=$_POST['id_area_distrito'];

  if(isset($_POST['button']) && $_POST['button'] == 'Crear'){

    $detalleEmpleado= new modelDetalleEmpleado();

    $detalleEmpleado->setUsuario_deta_persona($User);
    $detalleEmpleado->setPass_deta_persona($Pass);
    $detalleEmpleado->setCodigo_empleado($codEmpleado);
    $detalleEmpleado->setId_persona($idPersona);
    $detalleEmpleado->setId_estado_empleado($idEstadoEmpleado);
    $detalleEmpleado->setId_area_distrito($idAreaDistrito);
    $detalleEmpleado->create();

  }elseif (isset($_POST['button']) && $_POST['button'] == 'Editar'){

    $detalleEmpleado= new modelDetalleEmpleado();

    $detalleEmpleado->setUsuario_deta_persona($User);
    $detalleEmpleado->setPass_deta_persona($Pass);
    $detalleEmpleado->setCodigo_empleado($codEmpleado);
    $detalleEmpleado->setId_persona($idPersona);
    $detalleEmpleado->setId_estado_empleado($idEstadoEmpleado);
    $detalleEmpleado->setId_area_distrito($idAreaDistrito);
    $detalleEmpleado->update($id);

  }elseif (isset($_POST['button']) && $_POST['button'] == 'Eliminar'){

    $detalleEmpleado= new modelDetalleEmpleado();
    $detalleEmpleado->delete($id);

    //header('Location:../../DAO_CAP/HelpDesk/model.solicitudCaso.php');
  }


 ?>
